var Bicicleta = require("../../models/bicicleta");
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

describe('Bicicleta API', ()=>{

        describe('GET BICICLETAS /', ()=>{
            it('Status 200', (done)=>{
                request.get(base_url, function(error, response, body) {
                    var result = JSON.parse(body);
                    expect(response.statusCode).toBe(200);
                    expect(result.bicicletas.length).toBe(0);
                    done();
                });
            });
        });
    
        describe('POST BICICLETAS /create', ()=> {
            it('STATUS 200', (done)=> {
                var header = {'content-type': 'application/json'};
                var bici = '{ "id" : 10, "color" : "Rojo", "modelo" : "Urbana", "lat" : -27.469234, "lng" : -58.965871}';
                request.post({
                    headers : header,
                    url : base_url + '/create',
                    body : bici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var unaBici = JSON.parse(body).bicicleta;
                    console.log(unaBici);
                    expect(unaBici.color).toBe("Rojo");
                    expect(unaBici.modelo).toBe("Urbana");
                    expect(unaBici.ubicacion[0]).toBe(-27.469234);
                    expect(unaBici.ubicacion[1]).toBe(-58.965871);
                    done();
                });
            });
        });

    });

// describe('Bicicleta Api', ()=>{
//     describe('GET Bicicletas / ', ()=>{
//         it('Status 200', ()=>{
//             expect(Bicicleta.allBicis.length).toBe(0);
//             var a = new Bicicleta(1, 'rojo', 'urbana', [-0.251439, -78.521316]);
//             Bicicleta.add(a);
//             request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
//                 expect(response.statusCode).toBe(200);

//             });
//         });
//     });

//     describe('POST Bicicletas /create ', ()=>{
//         it('Status 200', (done)=>{
//             var headers = {'content-type': 'application/json'};
//             var aBici = '{"id": 10, "color":"marron", "modelo":"urbana", "lat":-34, "lng": 54}';
//             request.post({
//                 headers : headers,
//                 url: 'http://localhost:3000/api/bicicletas/create',
//                 body: aBici
//             }, function(error, response,body){
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("marron");
//                 done();   
//             });
//         });
//     });

//     describe('PUT Bicicletas /{id}/update ', ()=>{
//         it('Status 200', (done)=>{
//             var headers = {'content-type': 'application/json'};
//             var aBici = '{"id": 11, "color":"rojo", "modelo":"urbana", "lat":-34, "lng": 54}';
//             request.put({
//                 headers : headers,
//                 url: 'http://localhost:3000/api/bicicletas/10/update',
//                 body: aBici
//             }, function(error, response,body){
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(11).color).toBe("rojo");
//                 done();   
//             });
//         });
//     });

//     describe('DELETE Bicicletas /delete ', ()=>{
//         it('Status 200', (done)=>{
//             var headers = {'content-type': 'application/json'};
//             var biciID = '{"id": 11}';
//             request.delete({
//                 headers : headers,
//                 url: 'http://localhost:3000/api/bicicletas/delete',
//                 body: biciID
//             }, function(error, response,body){
//                 expect(response.statusCode).toBe(204);
//                 expect(Bicicleta.allBicis.length).toBe(1);
//                 done();   
//             });
//         });
//     });
// });